/*
 ST7735   -> STM32:
 #1 LED   -> 3.3V
 #2 SCK   ->    6
 #3 SDA   ->    4
 #4 A0/DC ->   10
 #5 RESET ->   11
 #6 CS    ->    9
 #7 GND   ->  GND
 #8 VCC   -> 3.3V
*/

#define SCR_WD   128
#define SCR_HT   160
#include <SPI.h>
#include <Adafruit_GFX.h>

#if (__STM32F1__) // bluepill
#define TFT_CS  PA2
#define TFT_DC  PA1
#define TFT_RST PA0
#include <Arduino_ST7735_STM.h>
#else
#define TFT_CS 10
#define TFT_DC  8
#define TFT_RST 9
#include <Arduino_ST7735_Fast.h>
#endif

Arduino_ST7735 lcd = Arduino_ST7735(TFT_DC, TFT_RST, TFT_CS);

#define pixelBufferSize 5120  // 128 * 40 or 160 * 32
uint16_t pixelBuffer[pixelBufferSize];  // size = ~10'000 bytes (RAM) = width*height*2


void setup(void) 
{
  for (int i = 0; i < pixelBufferSize; ++i) {
    pixelBuffer[i] = 0x0000;
  }
  
  /*
  127, 159          0, 159

                         +
                         y
                         -

  127, 0    + x -     0, 0
            pins
  */
  lcd.init();
  
  Serial.begin(25000000);
  delay(200);
}

void loop()
{
  lcd.fillScreen(BLACK);

  while (Serial.read() != 'G') {
    delay(10);
    Serial.print('R');  // Ready!
  }

  while (true) {
    while (Serial.available() == 0) { ; }
    char startCode = Serial.read();
    if (startCode == 'E') {  // Restart
      break;
    }
    for (int section = 0; section < 4; ++section) {
      if (section == 4) {
        Serial.println('F');  // First section!
      } else {
        Serial.println('G');  // Go! (Start sending image data)
      }
      for (int i = 0; i < pixelBufferSize; ++i) {
        while (Serial.available() < 2) { ; }
        pixelBuffer[i] = Serial.read();
        pixelBuffer[i] = (pixelBuffer[i] << 8) + Serial.read();
      }
      lcd.drawImage(section * 32, 0, 32, 160, pixelBuffer);
    }
  }
}
