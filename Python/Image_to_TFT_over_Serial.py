import serial
from PIL import Image, ImageOps
from PIL.ImageGrab import grab
import multiprocessing as mp  # reference: https://pymotw.com/2/multiprocessing/communication.html
from time import sleep, time


"""
Waiting for device...
Ready!
Starting!

Serial closed!
Total time:         43.0920000076s
  Image handling:   12.6660003662s
  Buffer handling:  6.67999792099s
  Serial handling:  22.1020002365s
Unaccounted time:   1.64400148392s
"""




class imgGrabber(mp.Process):  # Grabs screen and resizes it
  width = 160
  height = 128
  def __init__(self, input_queue, output_queue):
    mp.Process.__init__(self)
    self.input_queue = input_queue
    self.output_queue = output_queue
  def run(self):
    proc_name = self.name[-1]
    imageN = 0
    while True:
      inp = self.input_queue.get()
      if (inp == "EXIT"):
        self.input_queue.task_done()
        break
      if (inp == "REQ"):
        img = grab().resize((self.width, self.height))
        self.input_queue.task_done()  # Required, or q.join() will hang
        self.output_queue.put(img)
        imageN += 1
      print "c {0}: {1} images grabbed total!".format(proc_name, imageN)
    return


class bufferMaker(mp.Process):  # Converts 160*128 Image into a buffer
  width = 160
  height = 128
  def __init__(self, input_queue, output_queue):
    mp.Process.__init__(self)
    self.input_queue = input_queue
    self.output_queue = output_queue
  def color(self, (r, g, b)):  # r5,g6,b5  (11,5,0)
    return ((r/8)<<11) + ((g/4)<<5) + (b/8)
  #def uncolor(i):
  #  return ((i/0b0000100000000000), (i/0b0000000000100000) % 0b0000100000000000, i % 0b0000000000100000)
  def imgToBuffers(self, img):
    pix = ImageOps.flip(img).load()
    buffers = []
    for y_offset in range(self.height-1, -1, -32):  # 127, 95, 63, 31
      tempBuffer = ""
      col = 0
      for x in range(self.width-1, -1, -1):  # 156 ~ 0
        for y in range(32):
          #if (x == self.width-1):
          #  print "x= {0};y= {1}".format(x, y_offset - y)
          col = self.color(pix[x, y_offset - y])
          tempBuffer += chr(col / 256) + chr(col % 256)
      buffers.append(tempBuffer)
    return buffers
  def run(self):
    proc_name = self.name[-1]
    imgCopy = 0
    buffers = 0
    while True:
      inp = self.input_queue.get()
      if (type(inp) == str):
        if (inp == "EXIT"):
          self.input_queue.task_done()
          break
      else:
        imgCopy = inp  # This might just be a pointer, and not a copy...
        buffers = self.imgToBuffers(imgCopy)
        self.input_queue.task_done()  # Required, or q.join() will hang
        self.output_queue.put(buffers)
      print "c {0}: Buffer generated!".format(proc_name)
    return


class serialManager(mp.Process):  # Handles serial
  ser = serial.Serial()
  ser.port = "COM3"
  ser.baudrate = 25000000
  ser.timeout = 5
  readyChar = "R"
  firstChar = 'F'
  goChar = "G"
  doneChar = "D"
  resetChar = "E"  # Exit
  sections = 4
  def __init__(self, input_queue, output_queue):
    mp.Process.__init__(self)
    self.input_queue = input_queue
    self.output_queue = output_queue
  def run(self):
    proc_name = self.name[-1]
    bufferCopy = 0
    print "c {0}: Opening serial!".format(proc_name)
    while (not self.ser.isOpen()):
      try:
        self.ser.open()
      except serial.SerialException:
        sleep(2)
    print "c {0}: Waiting for device...".format(proc_name)
    while (self.ser.read() != self.readyChar):
      print "c {0}: ...".format(proc_name)
      self.ser.close()
      sleep(2)
      self.ser.open()
    print "c {0}: Ready!".format(proc_name)
    self.ser.write(self.goChar)
    print "c {0}: Starting!".format(proc_name)
    while True:  # Process loop
      try:
        inp = self.input_queue.get()
        if (type(inp) == str):
          if (inp == "EXIT"):
            self.ser.write(self.resetChar)
            self.ser.flush()
            self.ser.close()
            print "c {0}: Serial closed!".format(proc_name)
            self.input_queue.task_done()
            break
        else:
          bufferCopy = inp
          section = 0
          self.ser.write(self.goChar)
          while (section < self.sections):
            temp = self.ser.read(1)
            if (temp == self.firstChar):
              if (section != 0):
                print "c {0}: WARNING! SECTION RESET!  section = {1}".format(proc_name, section)
              section = 0
            if (temp == self.goChar or self.firstChar):
              self.ser.write(bufferCopy[section])
            section += 1
          self.ser.flush()
          self.input_queue.task_done()  # Required, or q.join() will hang
      finally:
        self.output_queue.put("EXIT")
    return


class deadman(mp.Process):
  history = []
  fuseActive = True
  logEntry = 0
  def __init__(self, input_queue, historySize, fuse):
    mp.Process.__init__(self)
    self.input_queue = input_queue
    self.fuse = fuse
    self.historySize = historySize
  def dump(self):
    for item in self.history:
      if (item[1] != ""):
        print "d {0}: [{1}]: {2}".format(self.name[-1], item[0], item[1].replace('\n', "\nc {0}: [{1}]: ").format(self.name[-1], item[0]))
  def run(self):
    proc_name = self.name[-1]
    for slot in range(self.historySize):
      self.history.append((0, ""))
    while True:
      try:
        inp = None
        inp = self.input_queue.get(timeout=self.fuse)
      except:
        if (self.fuseActive):
          print "\nd DEADMAN TRIGGERED!\n"
          self.dump()
          break
      if (inp == "EXIT"):
        print "d {0}: Exiting!".format(proc_name)
        self.input_queue.task_done()
        #self.dump()
        break
      elif (type(inp) == str):
        if (inp != ""):
          self.history.pop(0)
          self.history.append((self.logEntry, inp))
          self.logEntry += 1
        self.input_queue.task_done()  # Required, or you can't q.join()
      elif (type(inp) == bool):
        self.fuseActive = inp
        self.input_queue.task_done()
    return



def loggerOpen(q):
  global logger
  logger = q

def log(message):
  global logger
  logger.put(message)

def forceEmptyQ(q):
  try:
    while True:
      q.task_done()
  except ValueError:
    pass
  try:
    while True:
      q.get_nowait()
  except:
    pass

if (__name__ == "__main__"):
  try:
    print "\n\n\n\n\nM Starting python!"
    imageHandlerQ = mp.JoinableQueue()
    bufferHandlerQ = mp.JoinableQueue()
    serialHandlerQ = mp.JoinableQueue()
    feedbackQ = mp.JoinableQueue()
    deadmanQ = mp.JoinableQueue()
    loggerOpen(deadmanQ)
    log(False)
    
    imgHandlerP = imgGrabber(imageHandlerQ, bufferHandlerQ)
    bufferHandlerP = bufferMaker(bufferHandlerQ, serialHandlerQ)
    serialManagerP = serialManager(serialHandlerQ, feedbackQ)
    deadmanP = deadman(deadmanQ, 25, 10)

    deadmanP.start()
    serialManagerP.start()
    bufferHandlerP.start()
    imgHandlerP.start()

    bufferHandlerQ.put(Image.open("./img0.png", mode='r').convert("RGB"))
    sleep(2.5)
    imageHandlerQ.put("REQ")
    sleep(5)
    exitCode = feedbackQ.get(block=True)  # Wait untill the SerialHandler requests exit
    feedbackQ.task_done()
    print "M Received the following message from SerialHandler: {0}".format(exitCode)
  except KeyboardInterrupt:
    print "M FORCE SHUTTING DOWN!"
  finally:
    print "M Shutting down!"
    log(True)  # Activate deadman's logger
    imageHandlerQ.put("EXIT")
    bufferHandlerQ.put("EXIT")
    serialHandlerQ.put("EXIT")
    log("Waiting for SerialHandler to request exit.")
    exitCode = feedbackQ.get(block=True)  # Wait untill the SerialHandler requests exit
    feedbackQ.task_done()
    log("Received the following message from SerialHandler: {0}".format(exitCode))

    try:
      log("Joining P:  serialManagerP")
      serialManagerP.join()
      log("Joining P:  bufferHandlerP")
      bufferHandlerP.join()
      log("Joining P:  imgHandlerP")
      imgHandlerP.join()
      log("Joins P:   Done!")
      print "M JoinsP Done"
    except:
      print "M One or more processes either couldn't be closed, or already were closed!"
    try:
      log("qsizes:\n  imageHandlerQ.qsize = {0}\n  bufferHandlerQ.qsize = {1}\n  serialHandlerQ.qsize = {2}\n  feedbackQ.qsize = {3}".format(imageHandlerQ.qsize(), bufferHandlerQ.qsize(), serialHandlerQ.qsize(), feedbackQ.qsize()))
      log("Joining Q:  imageHandlerQ")
      imageHandlerQ.join()
      log("Joining Q:  bufferHandlerQ")
      bufferHandlerQ.join()
      log("Joining Q:  serialHandlerQ")
      serialHandlerQ.join()
      log("Joining Q:  feedbackQ")
      forceEmptyQ(feedbackQ)
      feedbackQ.join()
      log("Joins Q:   Done!")
      print "M JoinsQ Done!"
    except:
      print "M One or more queues either couldn't be closed, or already were closed!"
    print "M deadman closing!"
    deadmanQ.put("EXIT")
    deadmanQ.join()
    deadmanP.join()
    print "M deadman closed!"
    print "M Succesfull shutdown!"
