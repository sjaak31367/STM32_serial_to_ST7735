# [STM32] Serial to ST7735
Personal project to connect a small (128\*160) LCD to my computer as a small extra monitor.  


## Requirements:
Arduino IDE [1.8.13](https://www.arduino.cc/en/software)  
\- Roger's STM32 Library [latest](https://github.com/rogerclarkmelbourne/Arduino_STM32/wiki/Installation)  
\- cbm80amiga's ST7735 Library [link](https://github.com/cbm80amiga/Arduino_ST7735_STM)  
Python [2.7.9](https://www.python.org/downloads/release/python-279/)  
\- Pillow [5.2.0](https://github.com/python-pillow/Pillow/releases/tag/5.2.0) / [latest](https://pypi.org/project/Pillow/#files)<sup>1</sup>  
\- PySerial [3.4](https://github.com/pyserial/pyserial/releases/tag/v3.4) / [latest](https://pypi.org/project/pyserial/#files)  
<sup>1: Newer versions of Pillow seem to be for Python 3 only! (This repository was not written for Python 3!)</sup>

## Parts used:
| \#   | Part          | Specs                                | Link                                                         |  
| ---- | ------------- | ------------------------------------ | ------------------------------------------------------------ |  
|    1 | Maple Mini    | STM32F103CBT6, ROM: 128KB, RAM: 20KB | [[~$3.50](https://www.aliexpress.com/item/32292673542.html)] |  
|    1 | Display       | ST7735, 128\*160, 1.8"               | [[~$3.50](https://www.aliexpress.com/item/32843115817.html)] |  
|    8 | Jumper cables |                                      |                                                              |  
|    1 | Breadboard    |                                      |                                                              |  
